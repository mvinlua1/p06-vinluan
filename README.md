# Match Game (Project 6)
Marvin Vinluan (mvinlua1@binghamton.edu)  
29 July 2017

An implementation of a 'memory game' (match cards in a randomized field).

References:

https://gist.github.com/josephchang10/8b9e1b2ab1eb1e7c135dde96aec4c7c1
(shuffling an array)

https://stackoverflow.com/questions/11417077/changing-uibutton-text
(changing label of UIButton)
