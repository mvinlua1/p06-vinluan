//
//  BoardView.m
//  MatchGame
//
//  Created by Marvin Vinluan on 7/5/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BoardView.h"

@implementation BoardView
@synthesize cards;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        //
        cards = [[NSMutableArray alloc] init];
        CGRect bounds = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);

        self.isDone = false;
        self.state = 0;
        self.labelArray = [[NSMutableArray alloc] init];
        self.cardVals = [[NSMutableArray alloc] init];
        //self.cardNumVals = [[NSMutableArray alloc] init];
        self.cardState = [[NSMutableArray alloc] init];
        self.firstPick = nil;
        self.secondPick = nil;
        self.firstTag = 0;
        self.secondTag = 0;
        //self.totalMatches = 0;
        self.matchesLeft = 0;


        self.labelArray = @[@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H"];
        
        NSUInteger labelCount = [self.labelArray count];
        //self.totalMatches = labelCount;
        self.matchesLeft = labelCount;
        // go down baseArray and assign each element of labelArray twice
        for (NSUInteger i = 0; i < labelCount; ++i) {
            [self.cardVals addObject:@""];
            [self.cardState addObject:@NO];
            [self.cardState addObject:@NO];
            // assign label[i] to cardVals[2i]
            self.cardVals[2*i] = self.labelArray[i];
            // assign label[i] to cardVals[2i+1]
            self.cardVals[(2*i)+1] = self.labelArray[i];
        }
        NSUInteger count = [self.cardVals count];
        for (NSUInteger i = 0; i < count; ++i) {
            int nElements = count - i;
            int n = (arc4random() % nElements) + i;
            [self.cardVals exchangeObjectAtIndex:i withObjectAtIndex:n];
        }
        
    }
    return self;
}



- (IBAction)press:(id)sender {
    UIButton *button = (UIButton*)sender;

    if(self.state == 0) {
        // waiting for first pick
        // check if this button is already face up (ignore press if @YES)
        if([self.cardState[button.tag] isEqualToNumber:@NO]) {
            // show face of card and flip card to up (@YES)
            [button setTitle: self.cardVals[button.tag] forState: UIControlStateNormal];
            self.cardState[button.tag] = @YES;
            // save reference to property firstPick
            self.firstPick = button;
            self.firstTag = button.tag;
            [self.testLabel setText:@"Pick second card"];
            self.state = 1;
        }
    } else if(self.state == 1) {
        // waiting for second pick
        // check if this button is already face up (ignore press if @YES)
        if([self.cardState[button.tag] isEqualToNumber:@NO]) {
            // show face of card and flip card to up (@YES)
            [button setTitle: self.cardVals[button.tag] forState: UIControlStateNormal];
            //[self.cardVals[button.tag] = @YES;
            [self.cardState setObject:@YES atIndexedSubscript:button.tag];
            // save reference to property secondPick
            self.secondPick = button;
            self.secondTag = (int)button.tag;
            
            // NOW: compare the labels of firstPick, secondPick

            if([self.cardVals[self.firstTag] isEqualToString:self.cardVals[self.secondTag]]) {
                // match!
                // turn both cards white
                [self.firstPick setBackgroundColor:[UIColor whiteColor]];
                [self.secondPick setBackgroundColor:[UIColor whiteColor]];
                --self.matchesLeft;
                
                // if there are 0 matches left we're done...
                if(self.matchesLeft == 0) {
                    // we're done!
                    // color everything green
                    [self.button00 setBackgroundColor:[UIColor greenColor]];
                    [self.button01 setBackgroundColor:[UIColor greenColor]];
                    [self.button02 setBackgroundColor:[UIColor greenColor]];
                    [self.button03 setBackgroundColor:[UIColor greenColor]];
                    [self.button04 setBackgroundColor:[UIColor greenColor]];
                    [self.button05 setBackgroundColor:[UIColor greenColor]];
                    [self.button06 setBackgroundColor:[UIColor greenColor]];
                    [self.button07 setBackgroundColor:[UIColor greenColor]];
                    [self.button08 setBackgroundColor:[UIColor greenColor]];
                    [self.button09 setBackgroundColor:[UIColor greenColor]];
                    [self.button10 setBackgroundColor:[UIColor greenColor]];
                    [self.button11 setBackgroundColor:[UIColor greenColor]];
                    [self.button12 setBackgroundColor:[UIColor greenColor]];
                    [self.button13 setBackgroundColor:[UIColor greenColor]];
                    [self.button14 setBackgroundColor:[UIColor greenColor]];
                    [self.button15 setBackgroundColor:[UIColor greenColor]];
                    // advance to state 3, where any button touch resets/randomizes the board
                    [self.testLabel setText:@"You win! Touch any card to reset"];
                    self.state = 3;
                } else {
                    // otherwise go straight to state 0, we're ready to pick again
                    [self.testLabel setText:@"Match! Pick again"];
                    self.state = 0;
                }
            } else {
                // no match!
                [self.testLabel setText:@"Touch any card to try again"];
                // advance to state 2, where any button touch unflips cards and resets to state 0
                self.state = 2;
            }
        
        }
    } else if(self.state == 2) {
        // after there is no match
        [self.firstPick setTitle:@"" forState:UIControlStateNormal];
        [self.secondPick setTitle:@"" forState:UIControlStateNormal];
        self.cardState[self.firstTag] = @NO;
        self.cardState[self.secondTag] = @NO;
        self.firstTag = -1;
        self.secondTag = -1;
        [self.testLabel setText:@"Pick first card"];
        self.state = 0;
    } else if(self.state == 3) {
        // reset the board
        NSUInteger labelCount = [self.labelArray count];
        //self.totalMatches = labelCount;
        self.matchesLeft = labelCount;
        [self.cardVals removeAllObjects];
        [self.cardState removeAllObjects];
        // go down baseArray and assign each element of labelArray twice
        for (NSUInteger i = 0; i < labelCount; ++i) {
            [self.cardVals addObject:@""];
            [self.cardState addObject:@NO];
            [self.cardState addObject:@NO];
            // assign label[i] to cardVals[2i]
            self.cardVals[2*i] = self.labelArray[i];
            // assign label[i] to cardVals[2i+1]
            self.cardVals[(2*i)+1] = self.labelArray[i];
        }
        NSUInteger count = [self.cardVals count];
        for (NSUInteger i = 0; i < count; ++i) {
            int nElements = count - i;
            int n = (arc4random() % nElements) + i;
            [self.cardVals exchangeObjectAtIndex:i withObjectAtIndex:n];
        }
        [self.button00 setTitle: @"" forState: UIControlStateNormal];
        [self.button00 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button01 setTitle: @"" forState: UIControlStateNormal];
        [self.button01 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button02 setTitle: @"" forState: UIControlStateNormal];
        [self.button02 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button03 setTitle: @"" forState: UIControlStateNormal];
        [self.button03 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button04 setTitle: @"" forState: UIControlStateNormal];
        [self.button04 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button05 setTitle: @"" forState: UIControlStateNormal];
        [self.button05 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button06 setTitle: @"" forState: UIControlStateNormal];
        [self.button06 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button07 setTitle: @"" forState: UIControlStateNormal];
        [self.button07 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button08 setTitle: @"" forState: UIControlStateNormal];
        [self.button08 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button09 setTitle: @"" forState: UIControlStateNormal];
        [self.button09 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button10 setTitle: @"" forState: UIControlStateNormal];
        [self.button10 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button11 setTitle: @"" forState: UIControlStateNormal];
        [self.button11 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button12 setTitle: @"" forState: UIControlStateNormal];
        [self.button12 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button13 setTitle: @"" forState: UIControlStateNormal];
        [self.button13 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button14 setTitle: @"" forState: UIControlStateNormal];
        [self.button14 setBackgroundColor:[UIColor lightGrayColor]];
        [self.button15 setTitle: @"" forState: UIControlStateNormal];
        [self.button15 setBackgroundColor:[UIColor lightGrayColor]];
        [self.testLabel setText:@"Pick first card"];
        self.state = 0;
    }
    
}

@end

