//
//  BoardView.h
//  MatchGame
//
//  Created by Marvin Vinluan on 7/5/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardView.h"
#import "BoardView.h"
#import "Card.h"

#ifndef BoardView_h
#define BoardView_h

@interface BoardView : UIView

@property (nonatomic, strong) NSMutableArray *cards;

@property (strong, nonatomic) IBOutlet UILabel *testLabel;
@property (strong, nonatomic) IBOutlet UIButton *button00;
@property (strong, nonatomic) IBOutlet UIButton *button01;
@property (strong, nonatomic) IBOutlet UIButton *button02;
@property (strong, nonatomic) IBOutlet UIButton *button03;
@property (strong, nonatomic) IBOutlet UIButton *button04;
@property (strong, nonatomic) IBOutlet UIButton *button05;
@property (strong, nonatomic) IBOutlet UIButton *button06;
@property (strong, nonatomic) IBOutlet UIButton *button07;
@property (strong, nonatomic) IBOutlet UIButton *button08;
@property (strong, nonatomic) IBOutlet UIButton *button09;
@property (strong, nonatomic) IBOutlet UIButton *button10;
@property (strong, nonatomic) IBOutlet UIButton *button11;
@property (strong, nonatomic) IBOutlet UIButton *button12;
@property (strong, nonatomic) IBOutlet UIButton *button13;
@property (strong, nonatomic) IBOutlet UIButton *button14;
@property (strong, nonatomic) IBOutlet UIButton *button15;


@property (strong, nonatomic) NSMutableArray *labelArray;
@property (strong, nonatomic) NSMutableArray *cardVals;
@property (strong, nonatomic) NSMutableArray *cardState;
@property (strong, nonatomic) NSMutableArray *cardNumVals;
@property (nonatomic) bool isDone;
@property (nonatomic) int state;
@property (nonatomic) int totalMatches;
@property (nonatomic) int matchesLeft;
@property (nonatomic) UIButton* firstPick;
@property (nonatomic) UIButton* secondPick;
@property (nonatomic) int firstTag;
@property (nonatomic) int secondTag;

@end

#endif /* BoardView_h */
