//
//  ViewController.h
//  MatchGame
//
//  Created by Marvin Vinluan on 7/5/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <GameplayKit/GameplayKit.h>
#import "BoardView.h"
//#import "Card.h"
/*
typedef enum GameState : NSUInteger {
    gsStart,
    gsHasFirst,
    gsFinished
} GameState;
 */

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet BoardView *gameBoard;


/*
@property (strong, nonatomic) NSArray *baseArray;
@property (strong, nonatomic) NSMutableArray *buttons;
@property (strong, nonatomic) NSMutableArray *cardVals;
@property (strong, nonatomic) NSMutableArray *positions;
@property (nonatomic) bool isDone;
 */




@end

