//
//  CardView.m
//  MatchGame
//
//  Created by Marvin Vinluan on 7/6/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardView.h"

@implementation CardView

@synthesize cardLabel;
@synthesize cardColor;
@synthesize isUp;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        /*
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:label];
        [label setText:@"---"];
        [label setTextAlignment:NSTextAlignmentCenter];
        [self setBackgroundColor:[UIColor lightGrayColor]];
         */
        [self setBackgroundColor:[UIColor brownColor]];
    }
    return self;
}

@end
