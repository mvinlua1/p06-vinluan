//
//  Card.h
//  MatchGame
//
//  Created by Marvin Vinluan on 7/14/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef Card_h
#define Card_h

@interface Card : NSObject

@property (nonatomic, strong) UIButton *cardButton;
@property (nonatomic) NSString *cardLabel;
@property (nonatomic) bool isFaceUp;

@end

#endif /* Card_h */
