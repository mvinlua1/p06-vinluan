//
//  CardView.h
//  MatchGame
//
//  Created by Marvin Vinluan on 7/6/17.
//  Copyright © 2017 Marvin Vinluan. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef CardView_h
#define CardView_h

@interface CardView : UIView

@property (nonatomic, strong) NSString *cardLabel;
@property (nonatomic, strong) UIColor *cardColor;
@property (nonatomic) bool isUp;

@end

#endif /* CardView_h */
